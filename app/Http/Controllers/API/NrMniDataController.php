<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\NrMniData;
use App\NrMniDataCallCount;
use App\NrMniDataDuplicate;
use App\Setting;
use Validator;
use App\Events\FormSubmitted;
use App\User;
use DB;
class NrMniDataController extends Controller
{

   
    public function getOnCall() {

        $oncalls = NrMniData::where('status_call','=',2)->get();

        $datacount  = $oncalls->count();

        return response()->json(array('dataoncalls' => $oncalls, 'datacount' => $datacount)); 
    }

    public function countPending() {
              $NrMniData = NrMniDataDuplicate::where('diliver_status','=','pending')
                                ->orderBy('created_at', 'desc')
                                ->groupBy('nr_mni_data_id')
                                ->get();

        $datacounts = $NrMniData->count();

        return response()->json($datacounts);

    }
    //BUTTON ALL RESETS
    public function generalReset() { 

        $resetcall = NrMniData::where('status_call','=',2)->get();
        $statusoff = 1;
        $user_id = 0;

         if($resetcall->isEmpty()) {
              return response()->json('clearedStats');
         }

        foreach ($resetcall as $key => $resetcalls) {
                $resetcalls->status_call = $statusoff;
                $resetcalls->user_id = $user_id;
                $resetcalls->save();
               // status_call
                
        }


      return response()->json('successfullyReset');
    }

    // ======FRONT END FOR USER===
      // START CALL API ALGO d
    public function startCall(Request $request)
          {
            $user = auth('api')->user();

                    
                $Setting = Setting::where('id',1)->first();
                $datafdisp = array('0','04','11','12','13','08','09','17','18','96','97','98','99');
                           //01 03 05 07  22
                $datacollect = NrMniData::where('filesName',$Setting->filenameId)
                                        ->where('callPriority',$Setting->calls_set)
                                         ->whereIn('fdisp',$datafdisp)
                                        ->where('user_id','=',0)
                                         ->where('status_call','1')
                                         ->groupBy('id')
                                         ->first();
                if(!$datacollect) {
                     return response()->json('queCall');
                }else {
                    $userId = $user['id'];
                    $status_call = '2';
                    $statusupdate = NrMniData::where('id',$datacollect['id'])->first();
                    $statusupdate->status_call = $status_call;
                    $statusupdate->user_id = $userId;
                    $statusupdate->save();
                       
                    return response()->json($datacollect['id']);
                }

               
      

                  return response()->json($datacollect);
                 //return response()->json($NrMniData);
          } 



  
    public function updateDuplicate(Request $request) {
                          $fdspNumbers = $request->get('fdisp');
                          $result = substr($fdspNumbers, 0, 2);
                   $validators = Validator::make($request->all(),[
                         'nrdataidhide' => 'required | exists:nr_mni_datas,id'
                        ]); 
                     if ($validators->fails()) {
                        return response('error');
                    } 
                    $draftdata = '';
                    $nrDataDuplicate = new NrMniDataDuplicate([ 
                        'user_id'=> $request->get('userid'), 
                        'nr_mni_data_id'=> $request->get('nrdataidhide'), 
                        'compname'=> $request->get('compname'),
                        'compname_d'=> $request->get('compname_d'),
                        'akadba'=> $request->get('akadba'),
                        'akadba_d'=> $request->get('akadba_d'),
                        'physaddr'=> $request->get('physaddr'),
                        'physaddr_d'=> $request->get('physaddr_d'),
                        'physcity'=> $request->get('physcity'),
                        'physcity_d'=> $request->get('physcity_d'),
                        'physstate'=> $request->get('physstate'),
                        'physstate_d'=> $request->get('physstate_d'),
                        'physzip'=> $request->get('physzip'),
                        'physzip_d'=> $request->get('physzip_d'),
                        'vnumber'=> $request->get('vnumber'),
                        'vnumber_d'=> $request->get('vnumber_d'),
                        'loccount'=> $request->get('loccount'),
                        'loccount_d'=> $request->get('loccount_d'),
                        'products'=> $request->get('products'),
                        'products_d'=> $request->get('products_d'),
                        'exec1'=> $request->get('exec1'),
                        'exec1_d'=> $request->get('exec1_d'),
                        'gender1'=> $request->get('gender1'),
                        'gender1_d'=> $request->get('gender1_d'),
                        'officermail1'=> $request->get('officermail1'),
                        'title1'=> $request->get('title1'),
                        'title1_d'=> $request->get('title1_d'),
                        'exec2'=> $request->get('exec2'),
                        'exec2_d'=> $request->get('exec2_d'),
                        'gender2'=> $request->get('gender2'),
                        'gender2_d'=> $request->get('gender2_d'),
                        'officermail2'=> $request->get('officermail2'),
                        'title2'=> $request->get('title2'),
                        'title2_d'=> $request->get('title2_d'), 
                        'fnumber'=> $request->get('fnumber'),
                        'fnumber_d'=> $request->get('fnumber_d'),
                        'nnumber'=> $request->get('nnumber'),
                        'nnumber_d'=> $request->get('nnumber_d'),
                        'web'=> $request->get('web'),
                        'web_d'=> $request->get('web_d'),
                        'email'=> $request->get('email'),
                        'email_d'=> $request->get('email_d'),
                        'mailaddr'=> $request->get('mailaddr'),
                        'mailaddr_d'=> $request->get('mailaddr_d'),
                        'mailcity'=> $request->get('mailcity'),
                        'mailcity_d'=> $request->get('mailcity_d'),
                        'mailstate'=> $request->get('mailstate'),
                        'mailstate_d'=> $request->get('mailstate_d'),
                        'mailzip'=> $request->get('mailzip'),
                        'mailzip_d'=> $request->get('mailzip_d'),
                        'yearestab'=> $request->get('yearestab'),
                        'yearestab_d'=> $request->get('yearestab_d'),
                        'distribtyp'=> $request->get('distribtyp'),
                        'distribtyp_d'=> $request->get('distribtyp_d'),
                        'ownertype'=> $request->get('ownertype'),
                        'ownertype_d'=> $request->get('ownertype_d'),
                        'sales'=> $request->get('sales'),
                        'sales_d'=> $request->get('sales_d'),
                        'squarefeet'=> $request->get('squarefeet'),
                        'squarefeet_d'=> $request->get('squarefeet_d'),
                        'imports'=> $request->get('imports'),
                        'imports_d'=> $request->get('imports_d'),
                        'exec3'=> $request->get('exec3'),
                        'exec3_d'=> $request->get('exec3_d'),
                        'gender3'=> $request->get('gender3'),
                        'gender3_d'=> $request->get('gender3_d'),
                        'officermail3'=> $request->get('officermail3'),
                        'title3'=> $request->get('title3'),
                        'title3_d'=> $request->get('title3_d'),
                        'exec4'=> $request->get('exec4'),
                        'exec4_d'=> $request->get('exec4_d'),
                        'gender4'=> $request->get('gender4'),
                        'gender4_d'=> $request->get('gender4_d'),
                        'officermail4'=> $request->get('officermail4'),
                        'title4'=> $request->get('title4'),
                        'title4_d'=> $request->get('title4_d'),
                        'exec5'=> $request->get('exec5'),
                        'exec5_d'=> $request->get('exec5_d'),
                        'gender5'=> $request->get('gender5'),
                        'gender5_d'=> $request->get('gender5_d'),
                        'officermail5'=> $request->get('officermail5'),
                        'title5'=> $request->get('title5'),
                        'title5_d'=> $request->get('title5_d'),
                        'exec6'=> $request->get('exec6'),
                        'exec6_d'=> $request->get('exec6_d'),
                        'gender6'=> $request->get('gender6'),
                        'gender6_d'=> $request->get('gender6_d'),
                        'officermail6'=> $request->get('officermail6'),
                        'title6'=> $request->get('title6'),
                        'title6_d'=> $request->get('title6_d'),
                        'exec7'=> $request->get('exec7'),
                        'exec7_d'=> $request->get('exec7_d'),
                        'gender7'=> $request->get('gender7'),
                        'gender7_d'=> $request->get('gender7_d'),
                        'officermail7'=> $request->get('officermail7'),
                        'title7'=> $request->get('title7'),
                        'title7_d'=> $request->get('title7_d'),
                        'exec8'=> $request->get('exec8'),
                        'exec8_d'=> $request->get('exec8_d'),
                        'gender8'=> $request->get('gender8'),
                        'gender8_d'=> $request->get('gender8_d'),
                        'officermail8'=> $request->get('officermail8'),
                        'title8'=> $request->get('title8'),
                        'title8_d'=> $request->get('title8_d'),
                        'exec9'=> $request->get('exec9'),
                        'exec9_d'=> $request->get('exec9_d'),
                        'gender9'=> $request->get('gender9'),
                        'gender9_d'=> $request->get('gender9_d'),
                        'officermail9'=> $request->get('officermail9'),
                        'title9'=> $request->get('title9'),
                        'title9_d'=> $request->get('title9_d'),
                        'exec10'=> $request->get('exec10'),
                        'exec10_d'=> $request->get('exec10_d'),
                        'gender10'=> $request->get('gender10'),
                        'gender10_d'=> $request->get('gender10_d'),
                        'officermail10'=> $request->get('officermail10'),
                        'title10'=> $request->get('title10'),
                        'title10_d'=> $request->get('title10_d'),

                        // new fields

                        'exec11'=> $request->get('exec11'),
                        'exec11_d'=> $request->get('exec11_d'),
                        'gender11'=> $request->get('gender11'),
                        'gender11_d'=> $request->get('gender11_d'),
                        'officermail11'=> $request->get('officermail11'),
                        'title11'=> $request->get('title11'),
                        'title11_d'=> $request->get('title11_d'),
                        'exec12'=> $request->get('exec12'),
                        'exec12_d'=> $request->get('exec12_d'),
                        'gender12'=> $request->get('gender12'),
                        'gender12_d'=> $request->get('gender12_d'),
                        'officermail12'=> $request->get('officermail12'),
                        'title12'=> $request->get('title12'),
                        'title12_d'=> $request->get('title12_d'),
                        'exec13'=> $request->get('exec13'),
                        'exec13_d'=> $request->get('exec13_d'),
                        'gender13'=> $request->get('gender13'),
                        'gender13_d'=> $request->get('gender13_d'),
                        'officermail13'=> $request->get('officermail13'),
                        'title13'=> $request->get('title13'),
                        'title13_d'=> $request->get('title13_d'),
                        'exec14'=> $request->get('exec14'),
                        'exec14_d'=> $request->get('exec14_d'),
                        'gender14'=> $request->get('gender14'),
                        'gender14_d'=> $request->get('gender14_d'),
                        'officermail14'=> $request->get('officermail14'),
                        'title14'=> $request->get('title14'),
                        'title14_d'=> $request->get('title14_d'),
                        'exec15'=> $request->get('exec15'),
                        'exec15_d'=> $request->get('exec15_d'),
                        'gender15'=> $request->get('gender15'),
                        'gender15_d'=> $request->get('gender15_d'),
                        'officermail15'=> $request->get('officermail15'),
                        'title15'=> $request->get('title15'),
                        'title15_d'=> $request->get('title15_d'),
                        'cutdate'=> $request->get('cutdate'),
                        
                            // new fields

                        'parentname'=> $request->get('parentname'),
                        'parentname_d'=> $request->get('parentname_d'),
                        'parentcity'=> $request->get('parentcity'),
                        'parentcity_d'=> $request->get('parentcity_d'),
                        'parentstat'=> $request->get('parentstat'),
                        'parentstat_d'=> $request->get('parentstat_d'),
                        'onumber'=> $request->get('onumber'),
                        'onumber_d'=> $request->get('onumber_d'),
                        'header'=> $request->get('header'), 
                        'advertiser'=> $request->get('advertiser'), 
                        'primarysic'=> $request->get('primarysic'),
                        'sic2'=> $request->get('sic2'),
                        'companyid'=> $request->get('companyid'),
                        'datasource'=> $request->get('datasource'),
                        'contact'=> $request->get('contact'),
                        'odatetime'=> $request->get('odatetime'),
                        'ocommetns'=> $request->get('ocommetns'),
                        'tdatetime'=> $request->get('tdatetime'),
                        'tcomments'=> $request->get('tcomments'),
                        'fcomments'=> $request->get('fcomments'),
                        'fdatetime'=> $request->get('fdatetime'),
                        'operator'=> $request->get('operator'),
                        'fdisp'=> $result,
                        'confdate'=> $request->get('confdate'),
                        'bookid'=> $request->get('bookid'),
                        'newinyear'=> $request->get('newinyear'),
                        'listnum'=> $request->get('listnum'),
                        'pdatetime'=> $request->get('pdatetime'),
                        'pcomments'=> $request->get('pcomments'),
                        'qeflag'=> $draftdata,
                        'oagent'=> $request->get('oagent'),
                        'tagent'=> $request->get('tagent'),
                        'fagent'=> $request->get('fagent'),
                        'datetime4'=> $request->get('datetime4'),
                        'comments4'=> $request->get('comments4'),
                        'agent4'=> $request->get('agent4'),
                        'datetime5'=> $request->get('datetime5'),
                        'comments5'=> $request->get('comments5'),
                        'agent5'=> $request->get('agent5'),
                        'sysgencomments'=> $request->get('sysgencomments'),
                        'priority'=> $request->get('priority'),
                        'addresserror'=> $request->get('addresserror'),
                        'dpvfootnote'=> $request->get('dpvfootnote'),
                        'altphone'=> $request->get('altphone'),
                        'altphone_d'=> $request->get('altphone_d'),
                        'paddress'=> $request->get('paddress'),
                        'paddress_d'=> $request->get('paddress_d'),
                        'pzip5'=> $request->get('pzip5'),
                        'pzip5_d'=> $request->get('pzip5_d'),
                        'addrchgreason'=> $request->get('addrchgreason'),
                        'empchgreason'=> $request->get('empchgreason'),
                        'callcount'=>$request->get('datacounting'),
                        'filesName'=>$request->get('filesName'),
                        'agentsnotes'=>$request->get('agentsnotes'),
                        'callstart'=>$request->get('callstart')
                        
                    ]); 
                $nrDataDuplicate->save();
                $userStatus = '0';
                $dataId = $nrDataDuplicate['nr_mni_data_id'];
                $dataCount = NrMniData::where('id',$dataId)->first();
                $status = '1';
                $priority = $dataCount['callPriority'] + '1';
                $callPriority = $priority;
                $dataCount->fdisp = $result;
                $dataCount->callPriority = $callPriority;
                $dataCount->status_call = $status;
                $dataCount->user_id = $userStatus;
                $dataCount->save();
                 //UPDATE DATA TABLE
               
                $dataId = $nrDataDuplicate['nr_mni_data_id'];
                $countCall = NrMniDataDuplicate::where('nr_mni_data_id',$nrDataDuplicate->nr_mni_data_id)->count();
              return response()->json( array( 'dataupdated' => $nrDataDuplicate , 'countcall' => $countCall ));
    }
    //Check Status If Taking Calls
    public function updateStatusCalls(Request $request){

            $dataId = $request->input('nr_mni_data_id');
            $dataCount = NrMniData::where('id',$dataId)->first(); 
            $callstatus = $dataCount['status_call'];

            $validators = Validator::make($request->all(),[
                 'nr_mni_data_id' => 'required | exists:nr_mni_datas,id'
                ]); 

             if ($validators->fails()) {
                return response('error');
            } 
        
              if($callstatus == '2') {
                    $statusOne = '1';
                    $dataCount->status_call = $statusOne;
                    $dataCount->save();
                    return response()->json($dataCount['status_call']);
               }else{
                    return response()->json('correct');
               }
    }
    // ======ADMIN FUNCTION FOR USER===

    public function duplicateAllindex() {
                
                //DEVELOPEMENT TESTING DATA
        // $alldatanrMniData = NrMniDataDuplicate::chunk(2000, function($datas){  
        //                                  echo $datas;
        //                                  die; 
        //                                 }); 
     $alldatanrMniData = NrMniDataDuplicate::all();

        return response()->json($alldatanrMniData);
    }
    public function index() 
    {
        $NrMniDataUploaded = NrMniData::with('user')->chunk(10000, function($datas){  
                                         echo $datas;
                                         die; 
                                        });
      return response()->json($NrMniDataUploaded);

    }

    //PEEJAY NAA DIRE ELISE NING STATUS
    // from where('diliver_status','=','pending') TO where('diliver_status','=','EOP')

    public function nrmnidataDuplicateStatus() {

      $NrMniData = NrMniDataDuplicate::where('diliver_status','=','pending')
                                //->where('filesName','=','1602505094.csv')
                                // ->where('callcount','=','1')
                                ->groupBy('nr_mni_data_id')
                                // 
                                ->get()
                                ->unique('nr_mni_data_id');


        return response()->json(array("data"=>$NrMniData));
    }


    // public function nrmnidataDuplicateStatusnew() {

    //   $NrMniData = NrMniDataDuplicate::orderBy('created_at', 'desc')
    //                                         ->distinct('nr_mni_data_id')
    //                                         ->limit(30)
    //                                         ->get();
                               
    //     return response()->json(array("data"=>$NrMniData));
    // }


    public function nrmnidataDuplicateStatusEOP() {

      $NrMniDatas = NrMniDataDuplicate::where('diliver_status','=','eop')
                                ->orderBy('created_at', 'desc')
                                ->get()
                                ->unique('nr_mni_data_id')
                                ->values();
   
        return response()->json(array("data"=>$NrMniDatas));
    }

   public function getDelivered() {
     $NrMniData = NrMniDataDuplicate::where('diliver_status','=','delivered')
                                            ->orderBy('created_at', 'desc')
                                             ->chunk(10000, function($datas){  
                                                 echo   $datas->unique('nr_mni_data_id');
                                                    die;    
                                              });
     return response()->json($NrMniData);
   }

  public function getHistoryData() {
       $NrMniData = NrMniDataDuplicate::with('user')->get();
       return response()->json($NrMniData);
  }

  public function restoreDelivered(Request $request) {
   // $diliver_status = 'pending';
        $deliver_status = $request->input('diliver_status');
       $nr_mni_data_id = $request->input('nr_mni_data_id');
       $deliverid = $request->input('id');
       $getAll =  NrMniDataDuplicate::where('nr_mni_data_id', $nr_mni_data_id)->get();

      foreach ($getAll as $key => $getAlls) {
              $getAlls->diliver_status = $deliver_status;
              $getAlls->save();
          }

    return response()->json($getAll);
  } 
  

  public function deliveredStatus2() {

    $allDatas = NrMniDataDuplicate::all();
    $deliver_status = 'delivered';
    foreach ($allDatas as $key => $allData) {
              $allData->diliver_status = $deliver_status;
              $allData->save();
      }

    return response()->json($allData);
  }

  public function uploaded()
    {
        //
      $NrMniDataUploaded = NrMniData::all();
      return response()->json($NrMniDataUploaded);
    }

    public function show($id)
    {
        //
        $NrMniData = NrMniData::find($id);

        $countCall = NrMniDataDuplicate::where('nr_mni_data_id',$id)
                                            ->count();
        $dataChecking = NrMniDataDuplicate::where('nr_mni_data_id',$id)
                                            ->latest()->first();

        if(!$NrMniData) {
          return response()->json('Error');  
        }

        return response()->json( array( 'dataupdated' => $NrMniData , 'countcall' => $countCall, 'dataChecking' => $dataChecking ));
    }


    public function callCountsNr(Request $request)
    {
         
              $validators = Validator::make($request->all(),[
                         'nr_mni_data_id' => 'required | exists:nr_mni_datas,id',
                         'descriptions' => 'nullable',
                         'descriptions' => 'nullable',
                    ]); 
                 if ($validators->fails()) {
                    return response('error');
                }
                 $callCount = new NrMniDataCallCount([ 
                      'nr_mni_data_id' => $request->get('nr_mni_data_id'),
                 ]); 
                $callCount->save();

      return response()->json($callCount);
    }

     public function formSet(Request $request) {

            $forms_settings = $request->input('forms_settings');
              
            $validators = Validator::make($request->all(),[
                 'forms_settings' => 'required'    
            ]); 

            if ($validators->fails()) {
                return response('error2');
             }

             $settings = Setting::where('id',1)->first();
             $settings->forms_settings = $forms_settings;
             $settings->save();
            return response()->json($forms_settings);
      }

      public function setCalls(Request $request){


             $callset = $request->input('calls_set');
             $filenameId = $request->input('filenameId');
             $filenameData = $request->input('filenameData');
             $forms_settings = $request->input('forms_settings');

             $validators = Validator::make($request->all(),[
                     'calls_set' => 'required',
                     'forms_settings' => 'required',
                     'filenameId' => 'nullable | exists:nr_mni_datas,filesName'
                    
                ]); 

             if ($validators->fails()) {
                return response('error2');
             }
             $settings = Setting::where('id',1)->first();
             $settings->calls_set = $callset;
             $settings->filenameId = $filenameId;
             $settings->filenameData = $filenameData;
             $settings->forms_settings = $forms_settings;
             $settings->save();
             return response()->json($settings);
      }

      public function getsets() {
        $settings = Setting::where('id',1)->first();

        return response()->json($settings);
      }

      public function collectUploadedName() {

        $uploaded = NrMniData::select('filesName','filesNamePath')
                                    ->get()
                                    ->unique('filesName','filesNamePath');


        return response()->json($uploaded);
      }


      public function userInformation() {
          $user = auth('api')->user();
        return response()->json($user);

      }

        public function updateUserId(Request $request, $id)
        {
          $logs = auth('api')->user();
            $userId = $logs['id'];
            $udpateTalbeId = NrMniData::where('id', $id)->first();
            $user_id = $request->input('user_id');
            $udpateTalbeId->user_id = $user_id;
            $udpateTalbeId->save();

           return response()->json($udpateTalbeId);
        }


        public function updateStatus($id) {

             $getDataNr = NrMniData::where('id',$id)->first();
             $status = '1';
             $userStatus = NULL;
             $getDataNr->status_call = $status;
             $getDataNr->user_id = $userStatus;
             $getDataNr->save();

            return response()->json('success');
        }

        public function qeflag(Request $request, $id){
              $qeflagdata = NrMniDataDuplicate::where('id',$id)->first();
              $qeflag = $request->input('qeflag');
              $qeflagdata->qeflag = $qeflag;
              $qeflagdata->save();              
              return response()->json($qeflag);  
        }

        public function updateDuplicateData(Request $request, $id)

        {

            $updateData = NrMniDataDuplicate::where('id',$id)->first();

                $compname = $request->input('compname');
                $compname_d = $request->input('compname_d');
                $akadba = $request->input('akadba');
                $akadba_d = $request->input('akadba_d');
                $physaddr = $request->input('physaddr');
                $physaddr_d = $request->input('physaddr_d');
                $physcity = $request->input('physcity');
                $physcity_d = $request->input('physcity_d');
                $physstate = $request->input('physstate');
                $physstate_d = $request->input('physstate_d');
                $physzip = $request->input('physzip');
                $physzip_d = $request->input('physzip_d');
                $vnumber = $request->input('vnumber');
                $vnumber_d = $request->input('vnumber_d');
                $loccount = $request->input('loccount');
                $loccount_d = $request->input('loccount_d');
                $products = $request->input('products');
                $products_d = $request->input('products_d');
                $exec1 = $request->input('exec1');
                $exec1_d = $request->input('exec1_d');
                $gender1 = $request->input('gender1');
                $gender1_d = $request->input('gender1_d');
                $officermail1 = $request->input('officermail1');
                $title1 = $request->input('title1');
                $title1_d = $request->input('title1_d');
                $exec2 = $request->input('exec2');
                $exec2_d = $request->input('exec2_d');
                $gender2 = $request->input('gender2');
                $gender2_d = $request->input('gender2_d');
                $officermail2 = $request->input('officermail2');
                $title2 = $request->input('title2');
                $title2_d = $request->input('title2_d');
                $fnumber = $request->input('fnumber');
                $fnumber_d = $request->input('fnumber_d');
                $nnumber = $request->input('nnumber');
                $nnumber_d = $request->input('nnumber_d');
                $web = $request->input('web');
                $web_d = $request->input('web_d');
                $email = $request->input('email');
                $email_d = $request->input('email_d');
                $mailaddr = $request->input('mailaddr');
                $mailaddr_d = $request->input('mailaddr_d');
                $mailcity = $request->input('mailcity');
                $mailcity_d = $request->input('mailcity_d');
                $mailstate = $request->input('mailstate');
                $mailstate_d = $request->input('mailstate_d');
                $mailzip = $request->input('mailzip');
                $mailzip_d = $request->input('mailzip_d');
                $yearestab = $request->input('yearestab');
                $yearestab_d = $request->input('yearestab_d');
                $distribtyp = $request->input('distribtyp');
                $distribtyp_d = $request->input('distribtyp_d');
                $ownertype = $request->input('ownertype');
                $ownertype_d = $request->input('ownertype_d');
                $sales = $request->input('sales');
                $sales_d = $request->input('sales_d');
                $squarefeet = $request->input('squarefeet');
                $squarefeet_d = $request->input('squarefeet_d');
                $imports = $request->input('imports');
                $imports_d = $request->input('imports_d');
                $exec3 = $request->input('exec3');
                $exec3_d = $request->input('exec3_d');
                $gender3 = $request->input('gender3');
                $gender3_d = $request->input('gender3_d');
                $officermail3 = $request->input('officermail3');
                $title3 = $request->input('title3');
                $title3_d = $request->input('title3_d');
                $exec4 = $request->input('exec4');
                $exec4_d = $request->input('exec4_d');
                $gender4 = $request->input('gender4');
                $gender4_d = $request->input('gender4_d');
                $officermail4 = $request->input('officermail4');
                $title4 = $request->input('title4');
                $title4_d = $request->input('title4_d');
                $exec5 = $request->input('exec5');
                $exec5_d = $request->input('exec5_d');
                $gender5 = $request->input('gender5');
                $gender5_d = $request->input('gender5_d');
                $officermail5 = $request->input('officermail5');
                $title5 = $request->input('title5');
                $title5_d = $request->input('title5_d');
                $exec6 = $request->input('exec6');
                $exec6_d = $request->input('exec6_d');
                $gender6 = $request->input('gender6');
                $gender6_d = $request->input('gender6_d');
                $officermail6 = $request->input('officermail6');
                $title6 = $request->input('title6');
                $title6_d = $request->input('title6_d');
                $exec7 = $request->input('exec7');
                $exec7_d = $request->input('exec7_d');
                $gender7 = $request->input('gender7');
                $gender7_d = $request->input('gender7_d');
                $officermail7 = $request->input('officermail7');
                $title7 = $request->input('title7');
                $title7_d = $request->input('title7_d');
                $exec8 = $request->input('exec8');
                $exec8_d = $request->input('exec8_d');
                $gender8 = $request->input('gender8');
                $gender8_d = $request->input('gender8_d');
                $officermail8 = $request->input('officermail8');
                $title8 = $request->input('title8');
                $title8_d = $request->input('title8_d');
                $exec9 = $request->input('exec9');
                $exec9_d = $request->input('exec9_d');
                $gender9 = $request->input('gender9');
                $gender9_d = $request->input('gender9_d');
                $officermail9= $request->input('officermail9');
                $title9 = $request->input('title9');
                $title9_d = $request->input('title9_d');
                $exec10 = $request->input('exec10');
                $exec10_d = $request->input('exec10_d');
                $gender10 = $request->input('gender10');
                $gender10_d = $request->input('gender10_d');
                $officermail10 = $request->input('officermail10');
                $title10 = $request->input('title10');
                $title10_d = $request->input('title10_d');
                $parentname = $request->input('parentname');
                $parentname_d = $request->input('parentname_d');
                $parentcity = $request->input('parentcity');
                $parentcity_d = $request->input('parentcity_d');
                $parentstat = $request->input('parentstat');
                $parentstat_d = $request->input('parentstat_d');
                $onumber = $request->input('onumber');
                $onumber_d = $request->input('onumber_d');
                $header = $request->input('header');
                $advertiser = $request->input('advertiser');
                $primarysic = $request->input('primarysic');
                $sic2 = $request->input('sic2');
                $companyid = $request->input('companyid');
                $datasource = $request->input('datasource');
                $contact = $request->input('contact');
                $odatetime = $request->input('odatetime');
                $ocommetns = $request->input('ocommetns');
                $tdatetime = $request->input('tdatetime');
                $tcomments = $request->input('tcomments');
                $fcomments = $request->input('fcomments');
                $fdatetime = $request->input('fdatetime');
                $operator = $request->input('operator');
                $fdisp = $request->input('fdisp');
                $confdate = $request->input('confdate');
                $bookid = $request->input('bookid');
                $newinyear = $request->input('newinyear');
                $listnum = $request->input('listnum');
                $pdatetime = $request->input('pdatetime');
                $pcomments = $request->input('pcomments');
                $qeflag = $request->input('qeflag');
                $oagent = $request->input('oagent');
                $tagent = $request->input('tagent');
                $fagent = $request->input('fagent');
                $datetime4 = $request->input('datetime4');
                $comments4 = $request->input('comments4');
                $agent4 = $request->input('agent4');
                $datetime5 = $request->input('datetime5');
                $comments5 = $request->input('comments5');
                $agent5 = $request->input('agent5');
                $sysgencomments = $request->input('sysgencomments');
                $priority = $request->input('priority');
                $addresserror = $request->input('addresserror');
                $dpvfootnote = $request->input('dpvfootnote');
                $altphone = $request->input('altphone');
                $altphone_d = $request->input('altphone_d');
                $paddress = $request->input('paddress');
                $paddress_d = $request->input('paddress_d');
                $pzip5 = $request->input('pzip5');
                $pzip5_d = $request->input('pzip5_d');
                $addrchgreason = $request->input('addrchgreason');
                $empchgreason = $request->input('empchgreason');
                $agentsnotes =$request->input('agentsnotes');


                     // $validators = Validator::make($request->all(),[
                     //         'fdisp' => 'required'
                     //    ]); 

                    $updateData->compname = $compname;
                    $updateData->compname_d = $compname_d;
                    $updateData->akadba = $akadba;
                    $updateData->akadba_d = $akadba_d;
                    $updateData->physaddr = $physaddr;
                    $updateData->physaddr_d = $physaddr_d;
                    $updateData->physcity = $physcity;
                    $updateData->physcity_d = $physcity_d;
                    $updateData->physstate = $physstate;
                    $updateData->physstate_d = $physstate_d;
                    $updateData->physzip = $physzip;
                    $updateData->physzip_d = $physzip_d;
                    $updateData->vnumber = $vnumber;
                    $updateData->vnumber_d = $vnumber_d;
                    $updateData->loccount = $loccount;
                    $updateData->loccount_d = $loccount_d;
                    $updateData->products = $products;
                    $updateData->products_d = $products_d;
                    $updateData->exec1 = $exec1;
                    $updateData->exec1_d = $exec1_d;
                    $updateData->gender1 = $gender1;
                    $updateData->gender1_d = $gender1_d;
                    $updateData->officermail1 = $officermail1;
                    $updateData->title1 = $title1;
                    $updateData->title1_d = $title1_d;
                    $updateData->exec2 = $exec2;
                    $updateData->exec2_d = $exec2_d;
                    $updateData->gender2 = $gender2;
                    $updateData->gender2_d = $gender2_d;
                    $updateData->officermail2 = $officermail2;
                    $updateData->title2 = $title2;
                    $updateData->title2_d = $title2_d;
                    $updateData->fnumber = $fnumber;
                    $updateData->fnumber_d = $fnumber_d;
                    $updateData->nnumber = $nnumber;
                    $updateData->nnumber_d = $nnumber_d;
                    $updateData->web = $web;
                    $updateData->web_d = $web_d;
                    $updateData->email = $email;
                    $updateData->email_d = $email_d;
                    $updateData->mailaddr = $mailaddr;
                    $updateData->mailaddr_d = $mailaddr_d;
                    $updateData->mailcity = $mailcity;
                    $updateData->mailcity_d = $mailcity_d;
                    $updateData->mailstate = $mailstate;
                    $updateData->mailstate_d = $mailstate_d;
                    $updateData->mailzip = $mailzip;
                    $updateData->mailzip_d = $mailzip_d;
                    $updateData->yearestab = $yearestab;
                    $updateData->yearestab_d = $yearestab_d;
                    $updateData->distribtyp = $distribtyp;
                    $updateData->distribtyp_d = $distribtyp_d;
                    $updateData->ownertype = $ownertype;
                    $updateData->ownertype_d = $ownertype_d;
                    $updateData->sales = $sales;
                    $updateData->sales_d = $sales_d;
                    $updateData->squarefeet = $squarefeet;
                    $updateData->squarefeet_d = $squarefeet_d;
                    $updateData->imports = $imports;
                    $updateData->imports_d = $imports_d;
                    $updateData->exec3 = $exec3;
                    $updateData->exec3_d = $exec3_d;
                    $updateData->gender3 = $gender3;
                    $updateData->gender3_d = $gender3_d;
                    $updateData->officermail3 = $officermail3;
                    $updateData->title3 = $title3;
                    $updateData->title3_d = $title3_d;
                    $updateData->exec4 = $exec4;
                    $updateData->exec4_d = $exec4_d;
                    $updateData->gender4 = $gender4;
                    $updateData->gender4_d = $gender4_d;
                    $updateData->officermail4 = $officermail4;
                    $updateData->title4 = $title4;
                    $updateData->title4_d = $title4_d;
                    $updateData->exec5 = $exec5;
                    $updateData->exec5_d = $exec5_d;
                    $updateData->gender5 = $gender5;
                    $updateData->gender5_d = $gender5_d;
                    $updateData->officermail5 = $officermail5;
                    $updateData->title5 = $title5;
                    $updateData->title5_d = $title5_d;
                    $updateData->exec6 = $exec6;
                    $updateData->exec6_d = $exec6_d;
                    $updateData->gender6 = $gender6;
                    $updateData->gender6_d = $gender6_d;
                    $updateData->officermail6 = $officermail6;
                    $updateData->title6 = $title6;
                    $updateData->title6_d = $title6_d;
                    $updateData->exec7 = $exec7;
                    $updateData->exec7_d = $exec7_d;
                    $updateData->gender7 = $gender7;
                    $updateData->gender7_d = $gender7_d;
                    $updateData->officermail7 = $officermail7;
                    $updateData->title7 = $title7;
                    $updateData->title7_d = $title7_d;
                    $updateData->exec8 = $exec8;
                    $updateData->exec8_d = $exec8_d;
                    $updateData->gender8 = $gender8;
                    $updateData->gender8_d = $gender8_d;
                    $updateData->officermail8 = $officermail8;
                    $updateData->title8 = $title8;
                    $updateData->title8_d = $title8_d;
                    $updateData->exec9 = $exec9;
                    $updateData->exec9_d = $exec9_d;
                    $updateData->gender9 = $gender9;
                    $updateData->gender9_d = $gender9_d;
                    $updateData->officermail9= $officermail9;
                    $updateData->title9 = $title9;
                    $updateData->title9_d = $title9_d;
                    $updateData->exec10 = $exec10;
                    $updateData->exec10_d = $exec10_d;
                    $updateData->gender10 = $gender10;
                    $updateData->gender10_d = $gender10_d;
                    $updateData->officermail10 = $officermail10;
                    $updateData->title10 = $title10;
                    $updateData->title10_d = $title10_d;
                    $updateData->parentname = $parentname;
                    $updateData->parentname_d = $parentname_d;
                    $updateData->parentcity = $parentcity;
                    $updateData->parentcity_d = $parentcity_d;
                    $updateData->parentstat = $parentstat;
                    $updateData->parentstat_d = $parentstat_d;
                    $updateData->onumber = $onumber;
                    $updateData->onumber_d = $onumber_d;
                    $updateData->header = $header;
                    $updateData->advertiser = $advertiser;
                    $updateData->primarysic = $primarysic;
                    $updateData->sic2 = $sic2;
                    $updateData->companyid = $companyid;
                    $updateData->datasource = $datasource;
                    $updateData->contact = $contact;
                    $updateData->odatetime = $odatetime;
                    $updateData->ocommetns = $ocommetns;
                    $updateData->tdatetime = $tdatetime;
                    $updateData->tcomments = $tcomments;
                    $updateData->fcomments = $fcomments;
                    $updateData->fdatetime = $fdatetime;
                    $updateData->operator = $operator;
                    $updateData->fdisp = $fdisp;
                    $updateData->confdate = $confdate;
                    $updateData->bookid = $bookid;
                    $updateData->newinyear = $newinyear;
                    $updateData->listnum = $listnum;
                    $updateData->pdatetime = $pdatetime;
                    $updateData->pcomments = $pcomments;
                    $updateData->qeflag = $qeflag;
                    $updateData->oagent = $oagent;
                    $updateData->tagent = $tagent;
                    $updateData->fagent = $fagent;
                    $updateData->datetime4 = $datetime4;
                    $updateData->comments4 = $comments4;
                    $updateData->agent4 = $agent4;
                    $updateData->datetime5 = $datetime5;
                    $updateData->comments5 = $comments5;
                    $updateData->agent5 = $agent5;
                    $updateData->sysgencomments = $sysgencomments;
                    $updateData->priority = $priority;
                    $updateData->addresserror = $addresserror;
                    $updateData->dpvfootnote = $dpvfootnote;
                    $updateData->altphone = $altphone;
                    $updateData->altphone_d = $altphone_d;
                    $updateData->paddress = $paddress;
                    $updateData->paddress_d = $paddress_d;
                    $updateData->pzip5 = $pzip5;
                    $updateData->pzip5_d = $pzip5_d;
                    $updateData->addrchgreason = $addrchgreason;
                    $updateData->empchgreason = $empchgreason;
                    $updateData->agentsnotes = $agentsnotes;
                    $updateData->save();



            return response()->json($updateData);
        }

}

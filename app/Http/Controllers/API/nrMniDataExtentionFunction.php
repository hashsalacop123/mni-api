<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\NrMniData;
use App\NrMniDataCallCount;
use App\NrMniDataDuplicate;
use App\Setting;
use Validator;
use App\User;
use DB;


class nrMniDataExtentionFunction extends Controller
{
    //
     //dashboard checking super testing for the lastime
      public function dataComputation() {


       $uploaded = NrMniData::select('filesName','filesNamePath')
                                    ->get()
                                    ->unique('filesName','filesNamePath');
                     $datacouting = array();             
                    foreach ($uploaded as $key => $uploadeds){              
                    	 $datafilename = $uploadeds->filesName;
                    	 $filesNamePathdata = $uploadeds->filesNamePath;

                    	 $datacount  = NrMniData::where('filesName',$datafilename)->count();
                    	 $firstpass = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('callcount','0')->count();
                    	 $secondpass = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('callcount','0')->count();
                    	 $thirdpass = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('callcount','1')->count();
                    	 $firstcall = array('firstpass'=>$firstpass);

                    	 //ALL DISPO SECOND PASS
						 $secondpass01 = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('fdisp','01')
                    	 										->where('callcount','0')->count();

                    	 $secondpass03 = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('fdisp','03')
                    	 										->where('callcount','0')->count();
                    	 $secondpass05 = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('fdisp','05')
                    	 										->where('callcount','0')->count();
                    	 $secondpass06 = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('fdisp','06')
                    	 										->where('callcount','0')->count();
                    	 $secondpass07 = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('fdisp','07')
                    	 										->where('callcount','0')->count();
                    	 $secondpass10 = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('fdisp','10')
                    	 										->where('callcount','0')->count();
                    	 $secondpass22 = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('fdisp','22')
                    	 										->where('callcount','0')->count();

                    	 $seconddispo = $secondpass01 + $secondpass03 + $secondpass05 + $secondpass06 +  $secondpass07 + $secondpass10 + $secondpass22;
                    	 $resultSecond = $secondpass - $seconddispo;
                    	 if($resultSecond < 1) {
                    	 	$secondcall = array('secondpass'=>'0');
                    	 	
                    	 }else {
                    	 	$secondcall = array('secondpass'=>$resultSecond);
                    	 }

                    	 $thirdpass01 = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('fdisp','01')
                    	 										->where('callcount','1')->count();
                    	 $thirdpass03 = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('fdisp','03')
                    	 										->where('callcount','1')->count();

                    	 $thirdpass05 = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('fdisp','05')
                    	 										->where('callcount','1')->count();
                    	 $thirdpass06 = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('fdisp','06')
                    	 										->where('callcount','1')->count();
                    	 $thirdpass07 = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('fdisp','07')
                    	 										->where('callcount','1')->count();
                    	 $thirdpass10 = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('fdisp','10')
                    	 										->where('callcount','1')->count();
                    	 $thirdpass22 = NrMniDataDuplicate::where('filesName',$datafilename)
                    	 										->where('fdisp','22')
                    	 										->where('callcount','1')->count();

                    	 $thirddispo = 	$thirdpass01 + $thirdpass03 + $thirdpass05 + $thirdpass06 + $thirdpass07 + $thirdpass10 + $thirdpass22;

                    	 $resulthird = $thirdpass - $thirddispo;
						 if($resulthird < 1) {
                    	 	$thirdcall = array('thirdpass'=>'0');
                    	 	
                    	 }else {
                    	 	$thirdcall = array('thirdpass'=>$resulthird);
                    	 }


                    	 $datacouting[] = array("filename" => $datafilename, 
                    	 						"filepath" => $filesNamePathdata,
                    	 						 "computation" =>$datacount,
                    	 						 "firstcall"=>$firstcall,
                    	 						 "secondcall"=>$secondcall,
                    	 						 "thirdcall"=>$thirdcall
                    	 						);	
                    	  
                    }
                     
       return response()->json(array('data'=>$datacouting));

        //return response()->json(array('datafiles'=>$uploaded));
      }

      public function statusCount() {

      		 $delivered = NrMniDataDuplicate::where('diliver_status','=','delivered')->count();

      		 $pending = NrMniDataDuplicate::where('diliver_status','=','pending')->count();

      		 $eop = NrMniDataDuplicate::where('diliver_status','=','eop')->count();


      	return response()->json(array('delivered'=>$delivered, 'pending'=>$pending, 'eop'=> $eop));

      }

      public function retrievedOriginalData(Request $request){

      	$vnumber = $request->input('vnumber');

      	$originanl = NrMniData::where('vnumber',$vnumber)->get();
        $count = count($originanl);
      //	return response()->json($count);

      	if($originanl->isEmpty()){
      		return response()->json('empty');
      	}

      	return response()->json(array('originaldata' => $originanl, 'count' => $count));
      }


      public function individualOriginalRetrieve($id){

      		  $NrMniData = NrMniData::find($id);
      		 return response()->json($NrMniData);

      }



      public function individualDuplicateRetrieve($id){

      		  $NrMniData = NrMniDataDuplicate::find($id);

              $collection = collect($NrMniData);

             $filteredCollection = $collection->filter(function ($value) { return !is_null($value); });

         
      		 return response()->json($filteredCollection);
      }

      

       public function retrievedDuplicateData(Request $request){

      	$vnumber = $request->input('vnumber');

      	$originanl = NrMniDataDuplicate::where('vnumber',$vnumber)->orderBy('created_at', 'asc')->get();
        $count = count($originanl);
      //	return response()->json($count);

      	if($originanl->isEmpty()){
      		return response()->json('empty');
      	}

      	return response()->json(array('duplicateData' => $originanl, 'count' => $count));
      }

      public function updateStatus(Request $request) {

      		    $filesName = $request->get('filesName');
      		    $status = $request->get('diliver_status');

      		    $NrMniDataDuplicate = NrMniDataDuplicate::where('filesName',$filesName)->get();


      		    foreach ($NrMniDataDuplicate as $key => $NrMniDataDuplicates) {
      		    	         $NrMniDataDuplicates->diliver_status = $status;
               				 $NrMniDataDuplicates->save();
      		    }

      		   

      		   return response()->json($NrMniDataDuplicate);
      }

      public function getTitlePending() {

        $filenamepending = NrMniDataDuplicate::where('diliver_status','pending')->orderBy('created_at', 'desc')->first();
      
        $filenamepending->filesName;
        $dataquery = NrMniData::where('filesName',$filenamepending->filesName)->first();

        return response()->json( $dataquery->filesNamePath);
      }


}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNrMniDatas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
             Schema::table('nr_mni_datas', function (Blueprint $table) {;
               $table->string('user_id')->default(0)->change();
                $table->text('exec11', 200)->nullable();
                $table->text('gender11', 200)->nullable();
                $table->text('officermail11', 200)->nullable();
                $table->text('title11', 200)->nullable();
                $table->text('exec12', 200)->nullable();
                $table->text('gender12', 200)->nullable();
                $table->text('officermail12', 200)->nullable();
                $table->text('title12', 200)->nullable();
                $table->text('exec13', 200)->nullable();
                $table->text('gender13', 200)->nullable();
                $table->text('officermail13', 200)->nullable();
                $table->text('title13', 200)->nullable();
                $table->text('exec14', 200)->nullable();
                $table->text('gender14', 200)->nullable();
                $table->text('officermail14', 200)->nullable();
                $table->text('title14', 200)->nullable();
                $table->text('exec15', 200)->nullable();
                $table->text('gender15', 200)->nullable();
                $table->text('officermail15', 200)->nullable();
                $table->text('title15', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
